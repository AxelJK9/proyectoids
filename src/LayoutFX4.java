import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
/**
 *
 * @author Axel Tamayo
 */
public class LayoutFX4 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
          
        Button btn1= new Button("Button 1");
        Button btn2= new Button("Button 2");
        Button btn3= new Button("Button 3");
        Button btn4= new Button("Button 4");
        Button btn5= new Button("Button 5");
        
        GridPane pane = new GridPane();
        
        pane.setAlignment(Pos.CENTER);
        pane.setVgap(10);
        pane.setHgap(5);
        
        pane.add(btn1, 1, 4);
        pane.add(btn2, 2, 5);
        pane.add(btn3, 3, 4);
        pane.add(btn4, 4, 5);
        pane.add(btn5, 5, 4);   
        Scene scene = new Scene(pane, 500, 250);        
        primaryStage.setTitle("GridPane");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
