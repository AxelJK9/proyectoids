import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * @author Axel Tamayo
 */
public class LayoutFX1 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        
        Button btn1= new Button("Button 1");
        Button btn2= new Button("Button 2");
        Button btn3= new Button("Button 3");
        Button btn4= new Button("Button 4");
        Button btn5= new Button("Button 5");
        
        BorderPane root = new BorderPane();
        root.setTop(btn1);
        root.setBottom(btn2);
        root.setLeft(btn3);
        root.setRight(btn4);
        root.setCenter(btn5);
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("BorderPane");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
