package layoutfx;
import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

/**
 *
 * @author Axel Tamayo
 */
public class LayoutFX extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Button btn1= new Button("Button 1");
        Button btn2= new Button("Button 2");
        Button btn3= new Button("Button 3");
        Button btn4= new Button("Button 4");
        Button btn5= new Button("Button 5");
        
        FlowPane root = new FlowPane(Orientation.HORIZONTAL, 50, 30);
        root.getChildren().add(btn1);
        root.getChildren().add(btn2);
        root.getChildren().add(btn3);
        root.getChildren().add(btn4);
        root.getChildren().add(btn5);
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("FlowPane");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
