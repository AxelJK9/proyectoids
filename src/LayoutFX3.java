import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Axel Tamayo
 */
public class LayoutFX3 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        
        Button btn1= new Button("Button 1");
        Button btn2= new Button("Button 2");
        Button btn3= new Button("Button 3");
        Button btn4= new Button("Button 4");
        Button btn5= new Button("Button 5");
        
        VBox Vbox = new VBox(btn1, btn2, btn3, btn4, btn5);
        VBox vbox = new VBox(10);
        vbox.getChildren().addAll(new Button("Button 1"), new Button("Button 2"), new Button("Button 3"),  new Button("Button 4"),  new Button("Button 5"));
        
        
        Scene scene = new Scene(Vbox, 300, 250);
        
        primaryStage.setTitle("VBox");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
