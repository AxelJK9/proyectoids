import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author Axel Tamayo
 */
public class LayoutFX5 extends Application {

   @Override
   public void start(Stage primaryStage) throws Exception {
       AnchorPane root = new AnchorPane();
       Button button1 = new Button("Button 1");
       Button button2 = new Button("Button 2");
       Button button3 = new Button("Button 3");
       Button button4 = new Button("Button 4");


       AnchorPane.setTopAnchor(button1, 40.0);
       AnchorPane.setLeftAnchor(button1, 50.0);
       AnchorPane.setRightAnchor(button1, 70.0);

       AnchorPane.setTopAnchor(button2, 90.0);
       AnchorPane.setLeftAnchor(button2, 60.0);
       AnchorPane.setRightAnchor(button2, 350.0);

       AnchorPane.setTopAnchor(button3, 90.0);
       AnchorPane.setLeftAnchor(button3, 350.0);
       AnchorPane.setRightAnchor(button3, 70.0);

       AnchorPane.setTopAnchor(button4, 150.0);
       AnchorPane.setLeftAnchor(button4, 40.0);
       AnchorPane.setRightAnchor(button4, 50.0);
       

       root.getChildren().addAll(button1, button3, button2, button4);

       Scene scene = new Scene(root, 550, 250);

       primaryStage.setTitle("AnchorPane");
       primaryStage.setScene(scene);
       primaryStage.show();
   }

   public static void main(String[] args) {
       launch(args);
   }

}
