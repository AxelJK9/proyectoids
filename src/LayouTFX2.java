
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


/**
 *
 * @author Axel Tamayo
 */
public class LayouTFX2 extends Application {
    
    @Override
     public void start(Stage primaryStage) {
        Label lblTop = new Label ("Ingrese su nombre");
        Label lblBottom = new Label("Nombre requerido para acceso al foro");
        
        StackPane rootNode = new StackPane();
        
        StackPane.setAlignment(lblTop, Pos.TOP_CENTER);
        StackPane.setAlignment(lblBottom, Pos.BOTTOM_CENTER); 
        
        rootNode.getChildren().addAll( lblTop, lblBottom);
       
        Scene myScene = new Scene(rootNode, 250, 200);
        
        primaryStage.setScene(myScene);
        primaryStage.setTitle("StackPane");
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
