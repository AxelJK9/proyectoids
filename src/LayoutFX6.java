
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.scene.layout.HBox;

/**
 *
 * @author Axel Tamayo
 */
public class LayoutFX6 extends Application  {

  @Override
  public void start(Stage primaryStage) throws Exception { 
    primaryStage.setTitle("HBox");

    Button button1 = new Button("Button  1");
    Button button2 = new Button("Button  2");
    Button button3 = new Button("Button  3");
    Button button4 = new Button("Button  4");
    Button button5 = new Button("Button  5");


    HBox hbox = new HBox(button1, button2, button3, button4, button5);

    Scene scene = new Scene(hbox, 400, 200);
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  public static void main(String[] args) {
    Application.launch(args);
  }
}